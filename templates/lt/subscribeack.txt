Sveiki prisijungę prie diskusijų forumo %(real_name)s@%(host_name)s
%(welcome)s

Jei norite nusiųsti laišką forumo dalyviams, rašykite adresu:

	%(emailaddr)s

Bendra informacija apie forumą:

	%(listinfo_url)s

Jei kada nors norėsite pakeisti dalyvio nustatymus
arba atsisakyti dalyvavimo forume, apsilankykite tinklapyje:

	%(optionsurl)s

%(umbrella)s

Tie pats pakeitimai įmanomi ir siunčiant komandą el. pašto adresu:

	%(real_name)s-request@%(host_name)s

Komandų sąrašą su paaiškinimais gausite nusiuntę komandą 'help'.

Jūsų slaptažodis yra:

	%(password)s

Sistema paprastai kartą per mėnesį primena apie dalyvavimą %(host_name)s
forume,jei tai jums nepatiks - galėsite uždrausti slaptažodio priminimą.
Priminimo laiške siunčiama instrukcija kaip atsisakyti forumo bei kaip
pakeisti nustatymus.

Forumo tinklapyje bet kada galite paprašyti išsiųsti Jums slaptažodį.
