Turite patvirtinti atsijungimą nuo diskusijų forumo.

    Dalyvis: %(username)s
    Forumas: %(listname)s@%(hostname)s

Padarykite tai apsilankydami šiuo adresu:

    %(admindb_url)s
