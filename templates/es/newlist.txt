La lista de distribución `%(listname)s' se acaba de crear para usted.
A continuación se incluye información básica sobre su lista.

La clave para administrar la lista es:

    %(password)s

Necesita esta clave para configurar la lista. También es necesaria para
realizar tareas administrativas tales como aprobar los mensajes si decide
que la lista sea moderada. 

Puede configurar su lista en la siguiente página WEB:

    %(admin_url)s

La página para los usuarios de su lista es:

    %(listinfo_url)s

Puede incluso personalizar esas páginas a traves de la pagina de
configuración de la lista. Es necesario que tenga usted conocimientos
de HTML para poder hacerlo. 

También existe un mecanismo de gestión por correo electrónico para los
usuarios de la lista (no para los administradores). Para obtener 
información sobre su uso, se debe enviar un mensaje con la palabra
'help' como asunto (subject) o en el cuerpo del mismo, a: 

    %(requestaddr)s

Para anular la subscripción de un usuario: consulte la página 
'listinfo' y siga el enlace de la dirección del usuario o
introduzca la misma, como si usted fuera él. Utilice la clave de
adminstración en lugar de la clave del usuario. Tambien puede usar la
clave de administración para cambiar las opciones de los usuarios.

Por favor, remita sus dudas a %(siteowner)s.
