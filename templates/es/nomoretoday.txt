Se ha recibido un mensaje procedente de su dirección de correo electrónico
'%(sender)s' pidiendo una respuesta automática procedente de la lista de
distribución %(listname)s.  Se ha observado que has enviado %(num)s mensajes
hoy.  Para evitar problemas como los mensajes dentro de un bucle infinito
entre robots de correo electrónico, hoy no le mandaremos más mensajes.
Inténtelo mañana.

Si piensa que este mensaje está en un error, o si tiene cualquier otra
pregunta que hacer, por favor póngase en contacto con el propietario de la
lista en %(owneremail)s.
