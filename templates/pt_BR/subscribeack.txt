Bem Vindo a lista de discussão %(real_name)s@%(host_name)s!
%(welcome)s
Para postar nesta lista, envie uma mensagem para o endereço:

  %(emailaddr)s

Informações gerais sobre a lista de discussão podem ser encontradas em:

  %(listinfo_url)s

Se deseja se desinscrever ou modificar suas opções (eg, mudar de/para o 
modo digest, modificar a senha, etc.), visite sua página de inscrição 
em:

  %(optionsurl)s
%(umbrella)s
Você também pode fazer tais ajustes via e-mail enviando uma mensagem para:

  %(real_name)s-request@%(host_name)s

com a palavra 'help' no assunto ou corpo da mensagem (não inclua
as aspas), e você receberá uma mensagem com instruções.
Você deve saber sua senha para modificar as suas opções (incluindo
a própria senha) ou para se desinscrever. Ela é:

  %(password)s

Normalmente, o Mailman lhe lembrará de suas senhas em %(host_name)s 
uma vez por mês, mas pode desativar este recurso se preferir. Este 
lembrete também inclui instruções de como se desinscrever ou modificar 
suas opções de conta. Também existe um botão em sua página de opções 
que poderá lhe enviar a senha.
