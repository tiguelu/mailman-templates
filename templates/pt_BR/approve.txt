Sua requisição para o endereço %(requestaddr)s:

    %(cmd)s

foi direcionada para a pessoa que toma conta da lista.

Uma provável causa é a tentativa de inscrição em uma
lista "fechada".

Você receberá uma notificação da decisão do dono da lista sobre 
sua solicitação de inscrição. 

Qualquer questões sobre a política do dono da lista deverá ser 
enviada para:

    %(adminaddr)s
