Notificação de inscrição para a lista de discussão %(listname)s

Nós recebemos uma requisição%(remote)s para inscrição de seu endereço
de email, "%(email)s", para a lista de discussão %(listaddr)s. Para 
confirmar que deseja ser adicionado a esta lista de discussão, 
simplesmente responda a esta mensagem, mantendo o cabeçalho 
Assunto intacto. Ou visite esta página web:

    %(confirmurl)s

ou adicione as seguinte linha -- somente esta linha -- no 
corpo da mensagem para %(requestaddr)s:

    confirm %(cookie)s

Note que a simples resposta a esta mensagem deverá funcionar com 
a maioria dos leitores de email, pois eles deixam a linha de 
assunto da forma tradicional (a palavra "Re:" não causa problemas).

Se não deseja se inscrever nesta lista, desconsidere esta 
mensagem. Se achar que está sendo maliciosamente inscrito 
na lista, ou tem quaisquer outras questões, envie-as para 
%(listadmin)s.
