A sua autorização é requerida para a aprovação da inscrição
na lista de discussão:

    Para:  %(username)s
    Lista: %(listname)s@%(hostname)s

Quando lhe for conveniente, visite:

    %(admindb_url)s

para processar a requisição.
