Seu email para '%(listname)s' com o assunto

    %(subject)s

Esta em espera até que o moderador da lista revise-a para aprovação.

A razão de estar em espera é:

    %(reason)s

Ou a mensagem será postada a lista, ou receberá uma notificação da 
decisão do moderador. Se desejar cancelar esta postagem, visite 
o seguinte endereço:

    %(confirmurl)s
