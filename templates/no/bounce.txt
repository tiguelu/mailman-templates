Dette er en returmelding fra listesystemet Mailman:

    Liste:        %(listname)s
    Medlem:       %(addr)s
    Hendelse:     Medlemsskapet %(negative)s%(did)s.
    På grunn av:  Permanent retur eller for mye i retur
    %(but)s

%(reenable)s
Meldingen om retur er gjengitt nedenfor.

Spørsmål?
Kontakt Mailman systemadministrator på %(owneraddr)s.
