Følgende søknad om å få bli med på en liste må vurderes av deg:

    For:   %(username)s
    Liste: %(listname)s@%(hostname)s

Når du får tid, gå inn på:

    %(admindb_url)s

for å godkjenne eller avslå søknaden.
