Protože jste správcem konference, je požadován Váš souhlas s přihlášením
následujícího uživatele:

    Uživatel:   %(username)s
    email:      %(listname)s@%(hostname)s


Navštivte, dle Vašich možností, následující adresu:

    %(admindb_url)s

pro povolení nebo zamítnutí požadavku.
