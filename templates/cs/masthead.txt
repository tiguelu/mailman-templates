Příspěvky do konference  %(real_name)s zasílejte na adresu:
	%(got_list_email)s

Pro přihlášení nebo odhlášení použijte WWW rozhraní na adrese"
	%(got_listinfo_url)s
nebo použijte elektronickou poštu, zašlete slovo help v těle dopisu
na adresu
	%(got_request_email)s

Správce konference můžete nalézt na adrese
	%(got_owner_email)s

Když budete odpovídat na příspěvek, změňte, prosím, Subject: na 
něco rozumnějšího než je:
"Re: Contents of %(real_name)s digest..."
