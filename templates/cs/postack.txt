Vaše zpráva ve věci:

    %(subject)s

byla úspěšně doručena do konference %(listname)s.

Informace o konferenci: %(listinfo_url)s
Konfigurace Vašeho účtu: %(optionsurl)s
