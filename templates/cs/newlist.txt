Právě pro Vás byla vytvořena konference `%(listname)s'.
Následují informace o tom, jak s konferencí zacházet.

Vaše heslo, pro správu konference je:

    %(password)s

Toto heslo budete potřebovat pro konfiguraci konference, pro
vyřizování administrativních úkonů jako třeba akceptace přihlášení
účastníka do uzavřené konference nebo pro akceptaci příspěvků do
moderovaných konferencí.

Na následující adrese můžete nakonfigurovat svou konferenci.

    %(admin_url)s

Uživatelé budou ke konferenci přistupovat přes stránku: 

    %(listinfo_url)s

Vzhled těchto stránek můžete měnit dle své úvahy, ale budete
potřebovat alespoň základní znalosti HTML.

Uživatelé mohou využívat i emailové rozhraní
konference. Administrovat přes něj konferenci ale není možné. Jak se
toto rozhraní používá můžete zjistit zasláním příkazu help v těle
dopisu na tuto adresu:


    %(requestaddr)s

Pokud potřebujete odhlásit ručně účastníka konference, můžete tak
udělat přes stránku s informacemi o konferenci, kliknutím seznam
účastníků a potom na příslušnou e-mailovou adresu. Jako heslo použijte
své administrátorské heslo. Také můžete stejným způsobem nastavovat
účastníkům parametry jejich kont.

Pokud máte nějaké dotazy, zasílejte je na adresu
%(siteowner)s, pokud to ovšem není Vaše vlastní adresa.
