Tato zpráva je zasílána jednou měsíčně, aby Vás upozornila na stav
Vašeho členství v konferencích na serveru %(hostname)s
Zároveň obsahuje pro připomenutí hesla pro správu Vašeho členského
konta a informace o správě konference.

Pokud navštívíte níže uvedený odkaz, můžete si změnit parametry
svého členského konta, včetně přihlášení a odhlášení. 

Kromě http přístupu můžete parametry svého členského konta
měnit zasláním požadavků na '-request'adresu konference.
Dopis zaslaný (např. na, %(exreq)s) obsahující slovo 'help'
v těle dopisu vrátí zpět kompletní popis ovládání konference 
elektronickou poštou.

Pokud máte nějaké dotazy, problémy, připomínky a tak podobně,
zašlete je, prosím, na adresu %(owner)s.  Děkuji!

Hesla pro %(useraddr)s:



