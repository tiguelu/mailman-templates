Vaše emailová adresa  "%(email)s" byla pozvána do konference
%(listname)s, která je provozována na serveru
%(hostname)s. Správce konference %(listname)s
doufá, že pozvání přijmete.
Přijmout pozvání je jednoduché, stačí odpovědět na tuto
zprávu a nechat pole předmět: (Subject:) beze změny
(případné "Re:" doplněné  automaticky poštovním klientem nevadí),
a nebo můžete navštívit www stránku

    %(confirmurl)s

Třetí možností je odpovědět na adresu %(requestaddr)s
a ponechat v těle zprávy tento řádek: 
    confirm %(cookie)s

kromě něj by zpráva neměla obsahovat jiný text.
(Ve většině poštovních klientů prostě zašlete odpověď na tento dopis.
Obvykle je totiž řádek předmět: ponechán beze změny.)

Pokud se do konference přihlásit nechcete, nebo dokonce nevíte, proč jste
tento dopis dostali, prostě jej zahoďte. Pokud máte nějaký dotaz nebo
problém, prosím, kontaktujte správce na adrese: %(listowner)s.
