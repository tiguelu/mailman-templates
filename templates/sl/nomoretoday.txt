Z vašega e-poštnega naslova `%(sender)s' smo prejeli zahtevo za
samodejni odgovor s poštnega seznama %(listname)s. Danes smo s
tega naslova prejeli že %(num)s sporočil. Da bi se izognili težavam,
kot so poštne zanke v programih za e-pošto, vam danes ne bomo več
pošiljali takih odgovorov. Poskusite znova jutri.

Če menite, da je prišlo do napake, ali če imate kakšna vprašanja,
se obrnite na skrbnika seznama na naslov %(owneremail)s.

