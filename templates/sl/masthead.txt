Sporočila za poštni seznam od osebe %(real_name)s pošljite na
	%(got_list_email)s

Za prijavo ali odjavo s seznama prek interneta obiščite
	%(got_listinfo_url)s
prek e-pošte pa pošljite sporočilo z zadevo ali besedilom 'help' na
	%(got_request_email)s

Skrbnika poštnega seznama najdete na naslovu
	%(got_owner_email)s

Pri odgovarjanju oblikujete vrstico z Zadevo, tako da bo bolj opisna od
"Re: Vsebina izvlečka za %(real_name)s..."
