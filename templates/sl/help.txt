Pomoč za poštni seznam %(listname)s:

To je e-poštna ukazna pomoč za različico %(version)s poštnega
upravljalnika "Mailman". Sledijo ukazi, ki jih morate poslati,
če vas zanimajo informacije in nastavitve za vašo naročnino na
Mailman poštne sezname na tej strani. Ukaz vnesete v vrstico za
zadevo ali v besedilo sporočila.

Vedite, da je večina od naslednjih funkcij na voljo tudi na spletni
strani

    %(listinfo_url)s

Spletni naslov lahko uporabite tudi, če želite prejeti opomnik za geslo.

Ukaze, ki so omejeni na seznam (subscribe, who,...), pošljite na
*-request naslov posameznega seznama, npr. za seznam 'mailman' jih
pošljete na naslov 'mailman-request@...'.

V opisih ukazov besede v "<>" pomenijo OBVEZNE sestavine, besede v
"[]" pa predstavljajo NEOBVEZNE sestavine. Ko uporabljate ukaze,
ne vnašajte oklepajev <>[].

Veljavni so naslednji ukazi:

    %(commands)s

Ukaze pošljite na %(requestaddr)s

Vprašanja in komentarje, ki se jim mora posvetiti skrbnik, pošljite na

    %(adminaddr)s
