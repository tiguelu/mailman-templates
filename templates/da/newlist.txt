Maillisten `%(listname)s' er nu oprettet. Her følger lidt
grundlæggende information om listen.

Administrationsadgangskode er:

    %(password)s

Denne adgangskode er nødvendig for at konfigurere maillisten. Du skal
også bruge den for at behandle anmodninger til maillisten, f. eks.
godkende e-mail, hvis du vælger at begrænse listen til at være en
modereret liste.

Du kan konfigurere listen på følgende webside:

    %(admin_url)s

Websiden for tilmeldte til listen er:

    %(listinfo_url)s

Du kan også ændre i din personlige tilpasning af disse sider fra
listens konfigurationsside, men for at kunne gøre dette skal du
kunne skrive HTML-kode.

Der findes også en e-mailbaseret brugerflade for medlemmer (ikke
administratorer) af listen. Du kan få mere information om dette
ved at sende en e-mail til:

    %(requestaddr)s

kun indeholdende ordet 'help' i Emne feltet eller i selve
mailen.

Hvis et medlem af maillisten skal fjernes, kan du fra listens
web-side klikke på eller skrive medlemmets e-mailadresse. Der hvor
medlemmet skal skrive sin adgangskode skal du skrive din egen
administratoradgangskode.
Du kan f. eks. også stoppe udsendelse af listemail til dette medlem,
ændre til sammendrags-modus osv. på denne måde.

Du kan nu systemadminstrere på %(siteowner)s.
