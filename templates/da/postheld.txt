Du har sendt en e-mail til '%(listname)s' med emnet

    %(subject)s

Den holdes foreløbigt tilbage, indtil moderator har læst og
godkendt eller afvist din mail.

Begrundelsen for at tilbageholde din e-mail er:

    %(reason)s

Enten vil din e-mail blive videresendt til listen, eller du får den retur
med en begrundelse for afvisningen. Hvis du ønsker at
trække din e-mail tilbage, kan dette gøres på følgende URL:

    %(confirmurl)s
