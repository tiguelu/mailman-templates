Du er midlertidigt frameldt maillisten %(listname)s på grund af
%(reason)s.
Du vil ikke modtage flere mails fra listen før du igen tilmelder dig.
Du vil modtage %(noticesleft)s
påmindelser som denne før du bliver afmeldt listen.

For igen at modtage mail fra listen, skal du svare på denne mail
(med Subject: teksten som den er nu), eller gå ind på denne webside:

    %(confirmurl)s

Du kan også gå til din personlige medlemswebside:

    %(optionsurl)s

På denne side kan du også tilpasse leveringen af e-mail fra listen,
f. eks. ændre e-mail adresse og om du vil modtage mails enkeltvis eller
du vil have en e-mail med jævne mellemrum, hvori der er samlet flere e-mails.
Din adgangskode er:

    %(password)s

Har du spørgsmål eller problemer, kan du kontakte listens ejer på
følgende e-mailadresse:

    %(owneraddr)s
