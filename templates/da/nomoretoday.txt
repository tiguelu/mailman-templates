Vi har modtaget en anmodning fra din adresse '%(sender)s', som kræver
et automatisk svar fra maillisten %(listname)s. Vi har allerede fået %(num)s
anmodninger fra dig i dag. For at undgå problemer med e-mail der
sendes rundt i en uendelig løkke, vil vi ikke sende dig flere svar i dag.
Prøv venligst igen i morgen.

Kontakt venligst ejeren af maillisten på %(owneremail)s,
hvis du mener at dette ikke er korrekt, eller hvis du har spørgsmål.

