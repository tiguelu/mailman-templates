Feliratkozási kérelmed a(z) %(requestaddr)s listára:

    %(cmd)s

a lista üzemeltetőjéhez lett továbbítva.

Ez valószínűleg annak a következménye, hogy egy 'zártkörű' listára
próbáltál meg feliratkozni.

A lista üzemeltetőjének döntéséről a feliratkozási kérelmeddel 
kapcsolatban e-mailben értesítést fogsz kapni.

Észrevételeidet a lista politikájáról a következő címen tedd meg:

    %(adminaddr)s
