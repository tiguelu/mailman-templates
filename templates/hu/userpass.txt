Te, vagy valaki a nevedben, a %(fqdn_lname)s levelezőlistához tartozó
jelszavadat kérte ki e-mailben. A jelszóval a listatagsági beállításaidat
(mint pl. a levelek küldésének módját) tudod beállítani. A jelszó
ismeretében a listáról le is tudsz iratkozni.

A következő címmel vagy feliratkozva: %(user)s

A %(listname)s jelszavad a következő: %(password)s

A beállításaid megváltoztatásához jelszavad ismeretében lépj be a
következő weboldalra:

    %(options_url)s

Ugyanezeket e-mailen keresztül is el tudod végezni ezen a címen:

    %(requestaddr)s

A fenti címre egy "help" tartalmú levél elküldésével bővebb információt
lehet megtudni a beállításokról.

Kérdések, észrevételek? Küldj egy levelet a(z) %(listname)s levelezőlista
adminisztrátorának, a(z) %(owneraddr)s címre.
