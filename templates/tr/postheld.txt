'%(listname)s' mesaj listesine

    %(subject)s

konusuyla gönderdiğiniz mesaj, liste moderatörünün incelemesi ve
onaylaması için bekletiliyor.

Bekletilmesinin sebebi:

    %(reason)s

Ya mesaj listeye gönderilecek ya da moderatörün kararıyla ilgili bir
bildirim alacaksınız. Eğer bu mesajın gönderilmesinden vazgeçmek
isterseniz, lütfen şu URL'yi ziyaret edin:

    %(confirmurl)s

