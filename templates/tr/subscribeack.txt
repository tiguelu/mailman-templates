%(real_name)s@%(host_name)s mesaj listesine hoşgeldiniz!
%(welcome)s
Bu listeye mesaj atmak için şu adrese e-posta atın:

  %(emailaddr)s

Bu mesaj listesiyle ilgili genel bilgiyi burada bulabilirsiniz:

  %(listinfo_url)s

Eğer listeden çıkmak veya seçeneklerinizi değiştirmek isterseniz (örneğin
toplu veya ayrı ayrı mesajlar almak, şifrenizi değiştirmek, vb.) burada
üyelik sayfanızı ziyaret edebilirsiniz:

  %(optionsurl)s
%(umbrella)s
Bu tür ayarları e-postayla şu adrese mesaj göndererek de yapabilirsiniz:

  %(real_name)s-request@%(host_name)s

Konu başlığına veya mesaj gövdesine `help' sözcüğünü (tırnaklar olmadan)
yazarsanız açıklamaları içeren bir mesaj alacaksınız.

Seçeneklerinizi değiştirmek için (şifrenizin değişmesi dahil) veya
üyelikten çıkmak için şifrenizi bilmeniz gereklidir. Şifreniz:

  %(password)s

Normalde, Mailman size %(host_name)s mesaj listesi şifrelerinizi
ayda bir hatırlatır, ama isterseniz bunu devre dışı bırakabilirsiniz.
Bu hatırlatıcı, ayrıca listeden nasıl çıkacağınız veya hesap
seçeneklerinizi nasıl değiştireceğiniz ile iligili açıklamalar da
içerir. Ayrıca seçenekler sayfanızda şu andaki şifrenizi size
e-posta yoluyla gönderecek bir düğme de bulunmaktadır.

