%(requestaddr)s adresine isteğiniz:

    %(cmd)s

listeyi yöneten kişiye iletildi.

Bunun nedeni büyük olasılıkla 'kapalı' bir listeye üye olmaya çalışıyor
olmanızdır.

Liste sahibinin, üyelik isteğinizle ilgili kararını içeren bir e-posta
bildirimi alacaksınız.

Liste sahibinin ilkeleriyle ilgili her soru şu adrese yöneltilmelidir:

    %(adminaddr)s

