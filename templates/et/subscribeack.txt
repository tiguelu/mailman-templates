Tere tulemast listi %(real_name)s@%(host_name)s!
%(welcome)s

Listi kirjutamiseks on aadress

  %(emailaddr)s

Taustinfot listi kohta saab aadressil

  %(listinfo_url)s

Tellimuse lõpetamiseks ja tellimuse profiili muutmiseks
(kirjade saatmise moodus, parooli vahetamine, jms)
on veebileht 

  %(optionsurl)s
%(umbrella)s

Kõiki neid toiminguid saab teha ka meili teel. Selleks
tuleb aadressile

  %(real_name)s-request@%(host_name)s

saata kiri, mille sisuks üksainus sõna - help

Listserver vastab sellele kasutusjuhendiga.

Profiili muutmiseks (k.a. parooli muutmiseks) või listi
tellimuse lõpetamiseks on vaja parooli. Teie parool on:

  %(password)s

Mailman saadab kord kuus meeldetuletusi kõigi teie
%(host_name)s listide tellimuste kohta, kuid soovi korral
saad nendest meeldetuletustest loobuda.
