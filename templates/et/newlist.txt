List nimega '%(listname)s' on loodud. Järgnev tekst annab lühi-
ülevaate listi seadistustest ja edasisest konfigureerimisest.

Listi parool on:

    %(password)s

Seda parooli läheb tarvis listi konfiguratsiooni muutmiseks. Sama
parooli kasutatakse ka administreerimiseks, näiteks kirja listi 
lubamiseks, kui tegemist on modereeritava listiga

Listi saab konfigureerida aadressilt:

    %(admin_url)s

Listi kasutajate jaoks on veebileht aadressil:

    %(listinfo_url)s

Mõlema lehe kujundust on võimalik muuta listi konfigureerimise
juures, kuid selleks peab tundma HTMLi.

Eksisteerib ka meilipõhine kasutajaliides listi kasutajatele
(mitte administraatoritele); selle kohta saab infot saates
kirja, mille sisus on ainult sõna "help" aadressile:

    %(requestaddr)s

Tellija aadressi kustutamiseks listist ava 'listinfo' veebileht,
seejärel klikkige vastavale aadressile või sisesta see tekstiväljale.
Parooliks sisestage listi administraatoriparool. Sealtsamast saab
muuta ka kõiki selle listi tellija seadistusi, k.a. kokkuvõtete
saatmine, tellimuse peatamine, jne.

Võimalikele küsimustele vastab ja probleeme lahendab
	%(siteowner)s
