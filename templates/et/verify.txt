Listi %(listname)s tellimuse kinnitus.

Keegi - tõenäoliselt sina ise - soovib aadressile 
"%(email)s" tellida listi %(listaddr)s.

%(remote)s

Kui sa tõesti soovid seda listi tellida, siis vasta
sellele kirjale jättes kirja teema ja sisu samaks
(Re: võib teema alguses olla).

Alternatiivina saad tellimust kinnitada veebilehel

    %(confirmurl)s

Kolmas võimalus on saata vastus sellele kirjale aadressile
%(requestaddr)s, sel juhul peaks kirja sisu koosnema ühest
reast:

    confirm %(cookie)s

Kui sa ei soovi seda listi tellida, siis pole sul vaja
midagi teha ja sa võid selle kirja kustutada.

Kui sul on põhjust arvata, et keegi üritab seda listi
sulle tellida ilma sinu nõusolekuta, siis informeeri
sellest listi haldurit aadressil %(listadmin)s

Listi halduri poole võid pöörduda ka teiste selle listiga
seotud küsimustega.
