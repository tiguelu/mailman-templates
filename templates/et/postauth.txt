Listi administraatorina peate autoriseerima allpool kirjeldatud
kirja listi saatmise

    List:    %(listname)s@%(hostname)s
    Kellelt:    %(sender)s
    Teema: %(subject)s
    Põhjus:  %(reason)s

Kirja saab autoriseerida või kustutada aadressil

    %(admindb_url)s

