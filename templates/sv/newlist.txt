E-postlistan `%(listname)s' är nu upprättad. Här följer lite
grundläggande information om listan.

Administrationslösenordet är:

    %(password)s

Detta lösenord är nödvändigt för att konfigurera e-postlistan. Du måste
också ha det för att behandla förfrågningar till listan, till exempel
godkänna e-postbrev, om du väljer att att begränsa listan till att vara 
en modererad lista.


Du kan konfigurera listan på följande webbsida:

    %(admin_url)s

Webbsidan för medlemmar på listan är:

    %(listinfo_url)s

Du kan till och med göra din personliga anpassning för dessa sidor från
listans konfigurationssida, men då måste du kunna skriva HTML-kod.

Det finns också ett e-postbaserat gränssnitt för medlemmar (inte
administratörer) av listan. Du kan få mer information om detta genom 
att skicka ett e-postbrev till:

    %(requestaddr)s

med enbart ordet 'help' som innehåll eller i ämnesfältet.

För att ta bort någon från listan, gå in på listans webbsida och klicka på
eller skriv in medlemmens e-postadress. Där medlemmen skulle ha
skrivit in sitt lösenord, skriver du ditt administrationslösenord.
Du kan också till exempel stoppa distribution till en medlem, ändra till
sammandragsversion och så vidare, på denna sätt.

Du kan nå systemadminstratör på %(siteowner)s.
