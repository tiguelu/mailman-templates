Lista de discuĹŁii '%(listname)s' a fost creatÄ pentru dumneavoastrÄ.
Ăn continuare aveĹŁi cĂ˘teva informaĹŁii de bazÄ despre aceastÄ listÄ.

Parola dumnevoastrÄ de acces la listÄ este:

    %(password)s

VeĹŁi avea nevoie de aceastÄ parolÄ pentru a vÄ putea configura lista
de discuĹŁii.  De asemenea, parola vÄ va folosi la gestionarea cererilor
administrative, precum aprobarea mesajelor ĂŽn cazul unei liste moderate.

PuteĹŁi configura lista de discuĹŁii la urmÄtoarea adresÄ web:

    %(admin_url)s

Pagina de acces pentru utilizatorii listei dumneavoastrÄ este:

    %(listinfo_url)s

PuteĹŁi chiar sÄ personalizaĹŁi aceste pagini, prin intermediul paginii
de configurare a listei.  TotuĹi, vÄ sunt necesare cunoĹtinĹŁe HTML
pentru a putea face asta.

De asemenea, este disponibilÄ Ĺi o interfaĹŁÄ pentru utilizatori,
bazatÄ pe mesaje email; puteĹŁi astfel obĹŁine informaĹŁii despre modul
de folosire a listei, trimiĹŁĂ˘nd un mesaj ce are cuvĂ˘ntul 'help' ca
Ĺi subiect sau ca unic conĹŁinut, la adresa:

    %(requestaddr)s

Pentru a dezabona un utilizator: din pagina web 'listinfo' a listei,
faceĹŁi click pe adresa de email a utilizatorului, ca Ĺi cum aĹŁi fi acel
utilizator. Ăn locul parolei de utilizator, puneĹŁi parola dumneaoastrÄ de
administrare.  PuteĹŁi de asemenea, folosind parola de administrare, sÄ
modificaĹŁi opĹŁiunile utilizatorilor, incluzĂ˘nd rezumatele zilnice,
anularea livrÄrii, etc.

Va rugÄm sÄ adresaĹŁi toate ĂŽntrebÄrile cÄtre %(siteowner)s.
