Acesta este un mesaj lunar, de reamintire a datelor dumneavoastrÄ de acces
la listele de discuĹŁii de la %(hostname)s.  Mesajul include informaĹŁii
despre abonamente Ĺi cum sÄ modificaĹŁi aceste date sau cum sÄ pÄrÄsiĹŁi
o listÄ.

PuteĹŁi vizita adresele URL pentru a vÄ modifica opĹŁiunile personale sau
datele de abonament, inclusiv dezabonarea, setarea livrÄrii de rezumate
zilnice sau anularea totalÄ livrÄrii mesajelor (de ex. ĂŽn vacanĹŁ), Ĺi
aĹÄ mai departe.

Ăn plus, pe lĂ˘ngÄ aceste interfeĹŁe URL, mai puteĹŁi utiliza emailul pentru
a face schimbÄrile dorite.  Pentru mai multe detalii, trimiteĹŁi un email
la adresa '-request' a listei (de exemplu, %(exreq)s), conĹŁinĂ˘nd doar
cuvĂ˘ntul 'help' ca mesaj, Ĺi veĹŁi primi un rÄspuns automat cu instrucĹŁiuni.

DacÄ aveĹŁi ĂŽntrebÄri, nelÄmuriri, comentarii, etc, trimiteĹŁi-le la %(owner)s.
VÄ mulĹŁumim!

Parola pentru %(useraddr)s:
