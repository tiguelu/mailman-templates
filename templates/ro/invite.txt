Adresa dumneavoastrÄ "%(email)s" a fost aleasÄ pentru a vÄ invita sÄ
vÄ abonaĹŁi la lista de discuĹŁii %(listname)s de la %(hostname)s.
InvitaĹŁia este fÄcutÄ de cÄtre proprietarul listei %(listname)s.
PuteĹŁi accepta aceastÄ invitaĹŁie printr-un simplu rÄspuns (reply) la
acest mesaj, menĹŁinĂ˘nd intact cĂ˘mpul Subiect.

De asemenea, puteĹŁi vizita pagina web:

    %(confirmurl)s

sau puteĹŁi include urmÄtorul rĂ˘nd -- dar numai acesta --
ĂŽntr-un mesaj trimis la adresa %(requestaddr)s:

    confirm %(cookie)s

NotaĹŁi cÄ un simplu rÄspuns (reply) la acest mesaj funcĹŁioneazÄ din
marea majoritate a programelor de email.

DacÄ nu doriĹŁi sÄ onoraĹŁi aceastÄ invitaĹŁie, ignoraĹŁi acest mesaj.
DacÄ aveĹŁi orice alte ĂŽntrebÄri, vÄ rugÄm sÄ le trimiteĹŁi la
%(listowner)s.
