Vous, ou quelqu'un qui se fait passer pour vous, a demandé un
rappel de votre mot de passe d'abonné à la liste %(fqdn_lname)s.
Vous aurez besoin de ce mot de passe pour modifier vos options
d'abonnement (le choix entre les modes groupé et normal par exemple),
ce mot de passe vous facilite la tâche si vous voulez vous désabonner.

Vous êtes abonné avec l'adresse : %(user)s

Votre mot de passe pour la liste %(listname)s est : %(password)s

Pour modifier vos options d'abonnement, identifiez-vous et visitez
votre page d'options :

    %(options_url)s

Vous pouvez également procéder par mail en envoyant un message à :

    %(requestaddr)s

avec le mot "help" comme objet ou dans le corps du message, la réponse
automatique contiendra plus d'instructions.

Questions ou commentaires ? Veuillez les envoyer à l'administrateur
de la liste %(listname)s at %(owneraddr)s. 
