Votre abonnement à la liste %(listname)s a été désactivé suite à
%(reason)s. Vous ne recevrez plus de messages en provenance de
cette liste tant que vous n'aurez pas ré-activé votre
abonnement. Vous recevrez encore %(noticesleft)s rappels comme celui-ci 
avant que votre abonnement ne soit supprimé.

Pour ré-activer votre abonnement, vous pouvez répondre simplement à ce
message (en laissant la ligne Subject: --Objet-- du message intact) ou
vous rendre à la page de confirmation à l'adresse :

	 %(confirmurl)s

Vous pouvez également consulter votre page personnelle d'abonné à
l'adresse 

	%(optionsurl)s

Sur votre page personnelle d'abonné, vous pourrez modifier certaines
options de remise, tels votre adresse courriel ou le type de remise
selon que vous souhaitez recevoir les messages groupés ou
individuellement. Pour mémoire, votre mot de passe d'abonné est

	%(password)s

Si vous avez des questions ou des problèmes, contacter le
propriétaire de la liste  à l'adresse

	%(owneraddr)s
