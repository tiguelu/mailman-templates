Enviar mensagens de %(real_name)s para 
	%(got_list_email)s

Para se inscrever ou para anular a sua inscrição via web, visite o endereço
	%(got_listinfo_url)s
ou envie uma mensagem de email com a palavra 'help' no 
assunto ou no corpo da mensagem para 
	%(got_request_email)s

Pode entrar em contacto com a pessoa que gere a lista através do
endereço
	%(got_owner_email)s

Quando responder, por favor edite sua linha de assunto de forma a ela ser
mais específica do que
"Re: Contents of %(real_name)s digest..."
