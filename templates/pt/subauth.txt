A sua autorização é necessária para aprovar o pedido de inscrição na
lista de discussão:

    Para:  %(username)s
    Lista: %(listname)s@%(hostname)s

Para sua conveniência, visite:

    %(admindb_url)s

para processar o pedido.
